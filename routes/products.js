const express = require("express");
const router = express.Router();
const productController = require("../controllers/products");
const auth = require("../auth");

// Register A Product
router.post("/", auth.verify, (req, res) => {
	let items = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(items).then(resultFromController => res.send(resultFromController));
})

// Retrieve All Active Products
router.get("/", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
})

// Retrieve Specific Product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

// Update Product
router.put("/:productId", auth.verify, (req, res) => {
	const data = {
		product : req.body,
		productId : req.params.productId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	productController.updateProduct(data).then(resultFromController => res.send(resultFromController));
})

// Archive Product
router.put("/:productId/archive", auth.verify, (req, res) => {
	const archive = {
		product : req.body,
		productId : req.params.productId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	productController.archiveProduct(archive).then(resultFromController => res.send(resultFromController));
})

module.exports = router;



