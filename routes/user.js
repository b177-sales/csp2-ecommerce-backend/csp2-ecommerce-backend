const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

// Route for registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.put("/:userId/setAsAdmin", auth.verify, (req,res) => {
	let data = {
		userId : req.params.userId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	userController.setAsAdmin(data).then(resultFromController => res.send(resultFromController));
})

router.post("/products", auth.verify, (req,res) => {
	let data = {
		userId : req.params.userId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	userController.setAsAdmin(data).then(resultFromController => res.send(resultFromController));
})

// Post Check-Out
router.post("/checkout", auth.verify, (req, res) => {

	const data = {
		userId : req.body.userId,
		productId : req.body.productId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	userController.addProduct(data).then(resultFromController => res.send(resultFromController));
})

// Route for Retrieving User Orders
router.get("/orders", auth.verify, (req, res) => {
	const data = {
		userId : req.body.userId,
		productId : req.body.productId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	userController.getOrders(data).then(resultFromController => res.send(resultFromController));
});

// Route for Retrieving My Orders
router.get("/myOrders", auth.verify, (req, res) => {
	const data = {
		userId : req.body.userId,
		productId : req.body.productId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	userController.getMyOrders(data).then(resultFromController => res.send(resultFromController));
});


module.exports = router;