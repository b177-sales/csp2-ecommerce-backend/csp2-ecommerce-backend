const User = require("../models/User");
const Products = require("../models/Products");
const Orders = require("../models/Orders");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNum : reqBody.mobileNum,
		password : bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// User authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return { access : auth.createAccessToken(result)}
			}
			else{
				return Promise.resolve(`Password is incorrect.`);
			}
		}
	})
}

// Set user as admin (Admin only)
module.exports.setAsAdmin = (data) => {
	if (data.isAdmin) {
		let updateAdminField = {
			isAdmin : true
		};
		return User.findByIdAndUpdate(data.userId, updateAdminField).then((user, error) => {
			if (error) {
				return false;
			} 
			else {
				return Promise.resolve(`User is now an Admin.`);
			}
		});
	} 
	else {
		return Promise.resolve(`Can not proceed as user is not an Admin.`);
	}
};

// Non-admin User checkout (Create Order)
module.exports.addProduct = async(data) => {
    if(data.isAdmin == false) {
		let isUserUpdated = await User.findById(data.userId).then(user => {
            user.cart.push({productId : data.productId});
    
            return user.save().then((user, error) => {
                if(error) {
                    return false;
                }
                else{
                    return true;
                }
            })
        })
		let isProductUpdated = await Products.findById(data.productId).then(product => {
            product.orders.push({userId : data.userId});

            return product.save().then((product, error) => {
                if(error) {
                    return false;
                }
                else{
                    return true;
                }
            })
        })
        let isOrderUpdated = await Products.findById(data.productId).then(product => {
        	
        	let newOrder = new Orders({
                    userId: data.userId,
                    productId: data.productId,
                    purchasedOn: new Date()
                })
                return newOrder.save().then((product, error) => {
                    if(error){
                        return false;
                    }
                    else {
                        return true;
                    }
                })
        })
		if(isUserUpdated && isProductUpdated && isOrderUpdated) {
            return Promise.resolve(`User has created an order successfully.`);
        }
        else {
            return false;
        }
    }
    else {
        return Promise.resolve(`User is Admin.`);
    }     
}

// Retrieve all orders (Admin only)
module.exports.getOrders = (data) => {
	if(data.isAdmin) {
		return Orders.find({}).then(result => {
				return result;
		})
	}
	else {
		return Promise.resolve(`Data can't be accessed by non-Admin users.`);
	}
};

// Retrieve authenticated user’s orders
module.exports.getMyOrders = (data) => {
	return User.findById(data.userId).then(result => {
		if(data.isAdmin == false) {
			return result;
		}
		else {
			return Promise.resolve(`User is Admin.`);
		}
	});
};

