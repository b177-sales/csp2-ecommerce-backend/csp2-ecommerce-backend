const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Product is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	orders : [
		{
			userId : {
				type : String,
				default: true
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			},
			status: {
				type : String,
				default: "Added a new item."
			}
		}]
})

module.exports = mongoose.model("Products", productSchema);
