const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type : String,
		required : [true, "First Name is required"]
	},
	lastName: {
		type : String,
		required : [true, "Last Name is required"]
	},
	mobileNum: {
		type : String,
		required : [true, "Mobile Number is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	cart : [
		{
			userId : {
				type : String,
				default: true
			},
			productId : {
				type : String,
				default: true
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			},
			status: {
				type : String,
				default: "Has been added to cart."
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);