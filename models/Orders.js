const mongoose = require("mongoose");

const ordersSchema = new mongoose.Schema({
	userId : {
		type : String,
		default : true
	},	
	productId : {
		type : String,
		default : true
	},
	totalAmount : {
		type : Number,
		default : true
	},
	stock : {
		type : Number,
		default : true
	},
	purchasedOn : {
		type : Date,
		default : new Date()
	}
})

module.exports = mongoose.model("Orders", ordersSchema);